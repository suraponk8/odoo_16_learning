FROM odoo:16

COPY ./etc/requirements.txt ./



RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir -p /mnt/extra-addons
RUN mkdir -p /etc/odoo

COPY ./addons /mnt/extra-addons
COPY ./etc /etc/odoo