from odoo import api, fields, models

class Property(models.Model):
    _name = 'estate.property'
    _description = 'Estate Properties'

    name = fields.Char(string="Name" , required=True)

    type_id = fields.Many2one('estate.property.type',
        string='Property Type' 
    )

    
    tag_ids = fields.Many2many('estate.property.tag',
        string='Property Tag'
    )
    
    
    description = fields.Char(string="Description")
    postcode = fields.Char(string="Postcode")
    date_availability = fields.Date(
        string='Available From',
        default=fields.Date.context_today
    )
    expected_price = fields.Float(string='Expected Price')
    best_offer = fields.Float(string='Best Offer')
    selling_price = fields.Float(string='Selling Price')
    
    bedrooms = fields.Integer(string='Bedrooms')
    living_area = fields.Integer(string='Living Area (sqm)')
    facades = fields.Integer(string='Facades')
  
    garage = fields.Boolean(
      string='Garage', 
      default=False
    )
    garden = fields.Boolean(
      string='Garden', 
      default=False
    )
    garden_area = fields.Integer(string='Garden Area')
    
    garden_orientation = fields.Selection(
        string='Garden Orientation',
        selection=[('north', 'North'), ('south', 'South'),('east','East'),('west','West')],
        default='north'
        
    )

    
    offer_ids = fields.One2many(
        string='Offer',
        comodel_name='estate.property.offer',
        inverse_name='property_id',
    )
    
    sales_id = fields.Many2one(
        string='Salesman',
        comodel_name='res.users'
    )
    
    buyer_id = fields.Many2one(
        string='Buyer',
        comodel_name='res.partner',
        domain=[('is_company','=',True)]      
    )

    
    phone = fields.Char(
        string='Phone',
        related='buyer_id.phone'
    )
    
    
    # total_area = fields.Integer(
    #     string='Total Area',
    #     compute='_compute_total_area')
    
    # @api.depends('living_area','garden_area')
    # def _compute_total_area(self):
    #     for record in self:
    #         record.total_area = record.living_area + record.garden_area

  
    total_area = fields.Integer(string='Total Area')

    @api.onchange('living_area','garden_area')
    def _onchange_total_area(self):
            self.total_area = self.living_area + self.garden_area

    #onchange effect only within from view.
    
        
    
   
 


class PropertyType(models.Model):
    _name = 'estate.property.type'
    _description = 'Property Type'
    name = fields.Char(string="Name" , required=True)

class PropertyTags(models.Model):
    _name = 'estate.property.tag'
    _description = 'Property Tag'
    name = fields.Char(string="Name" , required=True)

#     #id, create_date , create_uid , write_date , write_uid    Odoo create auto
    
  
    
    

    

    
