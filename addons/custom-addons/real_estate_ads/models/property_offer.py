from odoo import api, fields, models
from odoo.exceptions import ValidationError
from datetime import timedelta


class PropertyOffer(models.Model):
    _name = 'estate.property.offer'
    _description = 'Estate Property Offers'

    
    price = fields.Float(
        string='Price',
    )
    
    status = fields.Selection(
        string='Status',
        selection=[('accepted', 'Accepted'), ('refused', 'Refused')]
    )
    
    partner_id = fields.Many2one(
        string='Customer',
        comodel_name='res.partner'   
    )
    
    property_id = fields.Many2one(
        string='Property',
        comodel_name='estate.property'
    )

    
    validity = fields.Integer(
        string='Validity',
    )

    
    deadline = fields.Date(string='Deadline',  compute = '_compute_deadline' ,
    inverse='_inverse_deadline'
    )

    # @api.model
    # def _set_create_date(self):
    #     return fields.Date.today()

    @api.model_create_multi
    def create(self,vals):
        for record in vals:
            if not record['creation_date']:
                record['creation_date'] = fields.Date.context_today
            return super(PropertyOffer,self).create(vals)
    

    creation_date = fields.Date(
        string='Creation Date',
        #default=_set_create_date,
        #default=fields.Date.context_today,
        
    )

    @api.depends('validity','creation_date')
    def _compute_deadline(self):
        for record in self:
            if record.creation_date:
                record.deadline = record.creation_date + timedelta(days=record.validity)
            else: 
                record.deadline  = False
    
    def _inverse_deadline(self):
        for record in self:
            if record.deadline and record.creation_date:
                record.validity = (record.deadline - record.creation_date).days
            else:
                record.validity = False

    @api.constrains('validity')
    def _check_validity(self):
        for record in self:
                if record.deadline <= record.creation_date:
                    raise ValidationError("Deadline can not be before creation date")
    
    
    # @api.multi
    # def write(self, values):

    #     print(values)
    #     res_partner_ids = self.env["res.partner"].search(
    #         ('is_company','=',True),
    #         ('name','=',values.get('name'))
    #     )
    #     print(res_partner_ids)
    #     result = super(PropertyOffer, self).write(values)
    
    #     return result
    
    
    
    # _sql_constraints = [
    #     (
    #         'check_validity',
    #         'check(validity > 0)',
    #         'Deadline can not be before creation date'
    #     )
    # ]
    


    
    # @api.autovacuum
    # def _clean_offers(self):
    #    self.search(['status','=','refused']).unlink()

  
    
      
    
    
    
    
    

    

    
    
    
    
    
    
    
    
    
    
    
    
